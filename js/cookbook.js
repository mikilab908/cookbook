let cardContainer = document.querySelector('#card-container');
let fullRecipeContainer = document.querySelector('#full-recipe-container');
let searchContainer = document.querySelector('#search-container');
let tagContainer = document.querySelector('#tag-container');
let header = document.querySelector('header');
let filtersContainer = document.querySelector('#filters');

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function formatTimeFromSecondsToHHMMSS(timeInSeconds) {
  let h = Math.floor(timeInSeconds / 3600);
  let m = Math.floor((timeInSeconds % 3600) / 60);
  let s = Math.floor(timeInSeconds % 60);

  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  s = s < 10 ? '0' + s : s;

  return `${h}:${m}:${s}`;
}

function createCard(cardData) {
  let card = document.createElement('div');
  let cimage = document.createElement('div');
  let title = document.createElement('h5');
  let contents = document.createElement('div');
  let actions = document.createElement('div');

  card.classList.add('card');

  cimage.classList.add('card-image');
  let img = document.createElement('img');
  img.src = './images/' + getRandomInt(1, 29) + '.jpg';
  cimage.appendChild(img);

  title.innerText = cardData.name;

  contents.classList.add('card-content');
  contents.appendChild(title);

  cardData.tags.forEach((tag) => {
    let tagLink = document.createElement('a');
    tagLink.innerText = '#' + tag;
    contents.appendChild(tagLink);

    tagLink.addEventListener('click', () => {
      location.hash = 'tag/' + tag;
    });
  });

  actions.classList.add('card-action');
  let link = document.createElement('a');
  link.innerText = 'Open Recipe';
  link.classList.add('waves-effect', 'waves-light', 'btn', 'orange');
  link.href = '#' + cardData.id;
  actions.appendChild(link);

  card.appendChild(cimage);
  card.appendChild(contents);
  card.appendChild(actions);

  return card;
}

function renderRecipe() {
  let id = location.hash.replace('#', '');
  let recipe = recipeData.find((r) => r.id === id);
  fullRecipeContainer.innerHTML = '';

  let wrapper = document.createElement('div');
  let card = document.createElement('div');
  let cimage = document.createElement('div');
  let title = document.createElement('h3');
  let contents = document.createElement('div');

  wrapper.classList.add('full-recipe-wrapper');

  title.innerText = recipe.name;

  cimage.classList.add('card-image');
  let img = document.createElement('img');
  img.src = './images/' + getRandomInt(1, 29) + '.jpg';
  cimage.appendChild(img);

  contents.classList.add('card-content');

  let prepTimeDiv = document.createElement('div');
  prepTimeDiv.innerHTML =
    'Prep time: ' + formatTimeFromSecondsToHHMMSS(recipe.preptime);

  let waitTimeDiv = document.createElement('div');
  waitTimeDiv.innerHTML =
    'Wait time: ' + formatTimeFromSecondsToHHMMSS(recipe.waittime);

  // Exercise 6 here - add ingredient list
  let ingredientsH4 = document.createElement('h4');
  ingredientsH4.innerText = 'Ingredients';
  let ingredientsList = document.createElement('ul');
  recipe.ingredients.forEach((ingredient) => {
    let li = document.createElement('li');
    li.innerHTML = ingredient;
    ingredientsList.appendChild(li);
  });

  // !!
  let p = document.createElement('p');
  p.innerText = recipe.instructions;
  let h4Instructions = document.createElement('h4');
  h4Instructions.innerText = 'Instructions';
  // Exercise 6 - append the new elements here
  // !!
  contents.appendChild(prepTimeDiv);
  contents.appendChild(waitTimeDiv);
  contents.appendChild(ingredientsH4);
  contents.appendChild(ingredientsList);
  contents.appendChild(h4Instructions);
  contents.appendChild(p);

  card.classList.add('card');
  card.appendChild(cimage);
  card.appendChild(contents);

  wrapper.appendChild(title);
  wrapper.appendChild(card);

  fullRecipeContainer.appendChild(wrapper);

  // Exercise 5 here - add a back "button"
  let backBtn = document.createElement('a');
  backBtn.classList.add('waves-effect', 'waves-light', 'btn', 'orange');
  backBtn.innerText = 'Back';

  backBtn.href = '#';
  wrapper.appendChild(backBtn);
  // !!
}

// Exercise 1 - draw cards on screen using the
// createCard function.
recipeData.length = 20;
recipeData.forEach((recipe) => {
  const card = createCard(recipe);
  cardContainer.appendChild(card);
});

// Exercise 2 - create a basic router
function handleRoute(e) {
  e.preventDefault();

  let hash = location.hash;
  console.log(hash);

  if (hash === '') {
    // On home page
    cardContainer.style.display = 'flex';
    fullRecipeContainer.style.display = 'none';
    tagContainer.style.display = 'none';
    searchContainer.style.display = 'none';
  } else {
    let isTag = hash.includes('tag');

    if (isTag) {
      // On Tags page

      let tag = location.hash.split('/')[1];
      let filteredRecipeData = recipeData.filter((recipe) =>
        recipe.tags.includes(tag)
      );
      tagContainer.innerHTML = '';
      filteredRecipeData.forEach((recipe) => {
        tagContainer.appendChild(createCard(recipe));
      });

      cardContainer.style.display = 'none';
      fullRecipeContainer.style.display = 'none';
      tagContainer.style.display = 'flex';
      searchContainer.style.display = 'none';
    } else if (hash.includes('search')) {
      // On Search page
      cardContainer.style.display = 'none';
      fullRecipeContainer.style.display = 'none';
      tagContainer.style.display = 'none';
      searchContainer.style.display = 'flex';

      let query = hash.split('/')[1];
      let filteredRecipes = recipeData.filter((recipe) => {
        return (
          recipe.tags.join(' ').indexOf(query) > -1 ||
          recipe.name.indexOf(query) > -1 ||
          recipe.instructions.indexOf(query) > -1
        );
      });

      searchContainer.innerHTML = '';
      filteredRecipes.forEach((recipe) => {
        let card = createCard(recipe);
        searchContainer.appendChild(card);
      });
    } else {
      // On Recipe Details page
      tagContainer.style.display = 'none';
      cardContainer.style.display = 'none';
      fullRecipeContainer.style.display = 'flex';
      searchContainer.style.display = 'none';
      renderRecipe();
    }
  }
}

function renderSearchForm() {
  let searchInput = document.createElement('input');
  searchInput.id = 'searchInput';

  let searchButton = document.createElement('button');
  searchButton.innerText = 'Search';
  searchButton.classList.add('waves-effect', 'waves-light', 'btn', 'orange');

  searchButton.addEventListener('click', searchRecipes);

  filtersContainer.appendChild(searchInput);
  filtersContainer.appendChild(searchButton);
}

function searchRecipes() {
  let query = document.querySelector('#searchInput').value;

  location.hash = 'search/' + query;
}

renderSearchForm();

window.addEventListener('hashchange', handleRoute);
window.addEventListener('load', handleRoute);

header.addEventListener('click', function () {
  location.hash = '';
});
